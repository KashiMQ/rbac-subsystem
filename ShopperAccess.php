<?php

include 'common_db.php';
include 'RBAC.php';
RBAC::init();

$dbo = db_connect();

$accessGroups = RBAC::listAccessGroups();

//temporary way to get shoppers.
//need listShoppers() from user / registration subsystem
$shoppers = array();
$query = "SELECT shopper.shopper_id, sh_username, sh_email, sh_firstname, sh_familyname from shopper, shaddr where shopper.shopper_id = shaddr.shopper_id";
$stmt = $dbo->query($query);

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	array_push($shoppers, $row);
}

?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
<link href="../css/shopper.css" rel="stylesheet">
<script src="../js/jquery-2.1.1.js"></script> 
<script src="../js/shoppers.js"></script> 
</head>

<body>
<header>
    <div class="container"><div class="logo">
          <a href="AccessGroups.php"><img src="../images/logo.jpg"></a>
      </div></div>
</header> 

<div class="body">
<div class="container">
	<div class="col-md-2">
		<div class="row">
			<a href="ShopperAccess.php" class="btn btn-primary btn-lg active">Shoppers</a>
			<a href="AccessGroups.php" class="btn btn-primary btn-lg">Access Groups</a>
		 </div>
      </div>

    <div class="col-md-7">
         <h2>Shoppers</h2>
         <h1>Select a shopper to modify their access groups.</h1>
	<div class="table-responsive">
	<table class="shopperTable table table-striped" >
	<tr>
		<th style="display:none;">Shopper ID</th>
		<th>Email</th>
		<th>First Name</th>
		<th>Last Name</th>
	</tr>
<?php 
for ($i = 0; $i < count($shoppers); ++$i) { ?>
	<tr>
	<td style="display:none;"> <?php echo $shoppers[$i]['SHOPPER_ID'] ?> </td>
	<td> <?php echo $shoppers[$i]['SH_EMAIL'] ?></td> 
	<td> <?php echo $shoppers[$i]['SH_FIRSTNAME'] ?> </td>
	<td><?php echo $shoppers[$i]['SH_FAMILYNAME'] ?></td>  
	</tr>
<?php } ?>

</table>
</div>
</div>
<h2 class="accessGroupMainHeader" style="display:none;">Access Groups</h2><div class="row">
<?php
for ($i = 0; $i < count($shoppers); ++$i) { ?>
  
	<h1 id="accessGroup<?php echo $shoppers[$i]['SHOPPER_ID']; ?>_Header" class="accessGroupHeader" style="display:none;">Access Groups for <?php echo $shoppers[$i]['SH_USERNAME']; ?></h1>
	<div class="table-responsive"><table id="accessGroup<?php echo $shoppers[$i]['SHOPPER_ID']; ?>_Table" class="accessGroupTable table table-striped"  style="display:none;">
		<tr>
			<th style="display:none;">Shopper ID</th>
			<th>Role Name</th>
			<th>Member</th>
		</tr>
    <?php
	for ($j = 0; $j < count($accessGroups); ++$j) {
		$shopperAccessGroups = RBAC::listShopperAccessGroups($shoppers[$i]['SHOPPER_ID']); ?>
		<tr>
		<td style="display:none;"> <?php echo $shoppers[$i]['SHOPPER_ID'] ?> </td>
		<td> <?php echo $accessGroups[$j]['AG_NAME'] ?> </td>
		<td><input type="checkbox" name="member" class="chkbox" value="<?php echo $accessGroups[$j]['AG_ID']; ?>" 
			 <?php foreach($shopperAccessGroups as $arr) { if (in_array($accessGroups[$j]['AG_ID'], $arr)){echo 'checked';}} ?> />
	    </tr>
	<?php } ?>
	</table></div>

<?php } 

?>
<div class="errorMessage"></div>
</div></div>
</div></div>
</body></html>