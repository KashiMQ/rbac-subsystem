<?php

class RBAC {

	public static $dbo;

	//calling this is required so the file can access the database
	public static function init() {
		require_once('common_db.php');
		self::$dbo = db_connect();
	}

	//F5 - list all access groups
	public static function listAccessGroups() {
		$result = array();
		$query = "SELECT * FROM AccessGroup";
		$stmt = self::$dbo->query($query);

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			array_push($result, $row);
		}

		return $result;
	}

	//F6 - list all shoppers for a specific access group
	public static function listAccessGroupShoppers($accessGroupID) {
		$result = array();
		$query = "SELECT AUG_Shopper_id, SH_USERNAME, SH_EMAIL, SH_FIRSTNAME, SH_FAMILYNAME FROM AccessUserGroup, SHOPPER, SHADDR
		          WHERE AUG_AG_id = " . $accessGroupID . "
		  	      AND AUG_Shopper_id = shopper.shopper_id 
		          AND Shaddr.shopper_id = shopper.shopper_id";
	    $stmt = self::$dbo->query($query);

	    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	    	array_push($result, $row);
	    }

	    return $result;
	}

	//F7 - list all access groups for a specific shopper
	public static function listShopperAccessGroups($shopperID) {
		$result = array();
		$query = "SELECT AG_id, AG_name, AG_desc FROM AccessGroup, AccessUserGroup
		          WHERE AUG_Shopper_id = " . $shopperID . " AND
		                AccessUserGroup.AUG_AG_id = AccessGroup.AG_id";
		$stmt = self::$dbo->query($query);

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			array_push($result, $row);
		}

		return $result;
	}

	//F8 - check if shopper can access a command
	public static function checkCommand($shopperID, $commandID) {
		$query = "SELECT AGC_id FROM AccessGroupCommands, AccessGroup, AccessUserGroup
		          WHERE AUG_Shopper_id = " . $shopperID . " AND 
		                AUG_AG_id = AG_id AND 
		                AG_id = AGC_AG_id AND
		                AGC_Cmd_id = " . $commandID;

		try {
			$stmt = self::$dbo->query($query);
			echo "User is allowed to access command.";
		} catch (PDOException $e) {
			echo $e->getMessage();
			return FALSE;
		}        
	}

	//F9 - register a shopper to access group ID 1
	public static function registerShopper($shopperID) {
		$query = "INSERT INTO AccessUserGroup (AUG_Shopper_id, AUG_AG_id)
		          VALUES (" . $shopperID . ", 1)";	          
		try {
			$stmt = self::$dbo->query($query);
			echo "User is now a Registered Shopper.";
		} catch (PDOException $e) {
			echo $e->getMessage();
			die ('Invalid Query');
		}

	}

	//F3 -- display all shoppers and their access groups
	public static function displayShoppersAccessGroups() {
		$result = array();
		$query = "SELECT sh_username, AG_id, AG_name, AG_desc FROM AccessUserGroup, AccessGroup, Shopper
		          WHERE AUG_Shopper_id = shopper_id AND AUG_AG_id = AG_id
		          ORDER BY sh_username";  	          
		$stmt = self::$dbo->query($query);

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			// $result = array(
			//     "sh_username" => $row['SH_USERNAME'],
			//     "AG_id" => $row['AG_ID'],
			//     "AG_name" => $row['AG_NAME'],
			//     "AG_desc" => $row['AG_DESC'],
		    // );

		    array_push($result, $row);

		}

		return $result;

	}

	//F4 -- display all access groups and their shoppers
	public static function displayAccessGroupsShoppers(){
		$result = array();
		$query = "SELECT AG_name, sh_username FROM AccessUserGroup, AccessGroup, Shopper
		          WHERE AUG_Shopper_id = shopper_id AND AUG_AG_id = AG_id
		          ORDER BY AG_name";  
  		$stmt = self::$dbo->query($query);

		while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
			// $result = array(
			//     "AG_name" => $row['AG_NAME'],
			//     "sh_username" => $row['SH_USERNAME'],
		    // );

		    array_push($result, $row);
		}

		return $result;
	}

	//F1 - add a specific shopper to a specific access group
	public static function addShopper($shopperID, $accessGroupID) {
		$query = "INSERT INTO AccessUserGroup (AUG_Shopper_id, AUG_AG_id)
	              VALUES (" . $shopperID . ", " . $accessGroupID . ")";	          
		try {
			$stmt = self::$dbo->query($query);
			echo "Success - Added " . $shopperID . " to access group: " . $accessGroupID;
		} catch (PDOException $e) {
			echo $e->getMessage();
			die ('Invalid Query');
		}
	}

	//F2 - remove a specific shopper from a specific access group
	public static function removeShopper($shopperID, $accessGroupID) {
		$query = "DELETE FROM AccessUserGroup
				  WHERE AUG_Shopper_id = " . $shopperID . " AND
				        AUG_AG_id = " . $accessGroupID;
		try {
			$stmt = self::$dbo->query($query);
			echo "Success - Deleted " . $shopperID . " from access group: " . $accessGroupID;
		} catch (PDOException $e) {
			echo $e->getMessage();
			die ('Invalid Query');
		}		     
	} 
}


?>