<html>
<head>
<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/style.css" rel="stylesheet">
</head>

<body>
<header>
    <div class="container"><div class="logo">
          <a href="AccessGroups.php"><img src="../images/logo.jpg"></a>
      </div></div>
</header> 

<div class="body">
<div class="container">
	<div class="col-md-2">
		<div class="row">
			<a href="ShopperAccess.php" class="btn btn-primary btn-lg">Shoppers</a>
			<a href="AccessGroups.php" class="btn btn-primary btn-lg active">Access Groups</a>
		 </div>
      </div>

<div class="col-md-10">     
<?php

include 'common_db.php';
include 'RBAC.php';
RBAC::init();

$dbo = db_connect();

$accessGroups = RBAC::listAccessGroups();

//temporary way to get shoppers.
//need listShoppers() from user / registration subsystem
$shoppers = array();
$query = "SELECT shopper.shopper_id, sh_username, sh_email, sh_firstname, sh_familyname from shopper, shaddr where shopper.shopper_id = shaddr.shopper_id";
$stmt = $dbo->query($query);

while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
	array_push($shoppers, $row);
}

//Each access group has its own table, containing users that belong to it
for ($i = 0; $i < count($accessGroups); ++$i) {
	$accessGroupShoppers = RBAC::listAccessGroupShoppers($accessGroups[$i]['AG_ID']); ?>
	<h2 class="accessGroupHeader"><?php echo $accessGroups[$i]['AG_NAME']; ?> Access Group</h2>
	<div class="table-responsive">
	<table class="table table-striped">
		<tr>
			<th>Email</th>
			<th>First Name</th>
			<th>Last Name</th>
		</tr>
		<?php for ($j = 0; $j < count($accessGroupShoppers); ++$j) { ?>
			<tr>
				<td><?php echo $accessGroupShoppers[$j]['SH_EMAIL'] ?></td> 
				<td><?php echo $accessGroupShoppers[$j]['SH_FIRSTNAME'] ?></td>
				<td><?php echo $accessGroupShoppers[$j]['SH_FAMILYNAME'] ?></td> 
			</tr>
		<?php } ?>
	</table>
	</div>
	<?php }

?>
</div><!-- div col-md-10 -->
</div></div><!-- div container and body -->
</body>
</html>