# RBAC Subsystem #
## COMP344 Assignment 2

This repository contains the source code for the roles-based access control subsystem implementation for the iRole group in COMP344.

## Wiki
Documentation for this solution can be found on [Assignment Two Wiki on iLearn](http://ilearn.mq.edu.au/mod/ouwiki/view.php?id=2543262&page=Role-Based+Access+Control).