
<html>

<?php 

//tests to see if RBAC is working

session_start();

include 'common_db.php';
include 'RBAC.php';
RBAC::init();

echo "listAccessGroups()<br/>";
$test = RBAC::listAccessGroups();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<br/>";
echo "listAccessGroupShoppers() -- lists all managers' userID<br/> ";
$test = RBAC::listAccessGroupShoppers(3);
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<br/>";
echo "listShopperAccessGroups() -- lists user 0's access groups<br/> ";
$test = RBAC::listShopperAccessGroups(0);
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<br/>";
echo "checkCommand() -- checks if a manager can add a product<br/> ";
$test = RBAC::checkCommand(3, 3);
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<br/>";
echo "displayShoppersAccessGroups()<br/> ";
$test = RBAC::displayShoppersAccessGroups();
echo '<pre>';
print_r($test);
echo '</pre>';

echo "<br/>";
echo "displayAccessGroupsShoppers()<br/> ";
$test = RBAC::displayAccessGroupsShoppers();
echo '<pre>';
print_r($test);
echo '</pre>';


?>

</html>
	

