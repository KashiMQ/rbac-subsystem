var xmlHttp = createXmlHttpRequestObject();
var currentCheckBox = null;

$(document).ready(function () 
{
	//load a div for displaying a progress animation cursor
    $('<div class="loadingBackground"><div class="loadingDiv"><img class="loadingImg" src="../images/loading.gif" alt="Loading..."/><h2 style="color:gray;">Please wait...</h2></div></div>').appendTo(document.body);
    $(".loadingBackground").css({ opacity: 0.35 });
});

/*
  Displays a progress animation cursor when processing 
  a server request
*/
function ShowProgressAnimation() 
{
	$(".loadingBackground").show();
}

/*
  Hides the progress animation cursor that is displayed when processing 
  a server request
*/
function HideProgressAnimation() 
{
	 //setTimeout(function(){
	 //      $(".loadingBackground").hide();}, 1000); 
	 $(".loadingBackground").hide();
}

$(function() 
{
	 //bind each row to a click event so that when the row is selected we can show the appropriate shopper access group table
    var tr = $('.shopperTable').find('tr');
    tr.bind('click', showShopperAccessGroup);

    //make the selected row in the shoppers grid a different style so it stands out
	tr.bind('click', function(event) 
	{         
          tr.removeClass('rowSelected');
          var tds = $(this).addClass('rowSelected').find('td');  
		  $('.errorMessage').html(""); //clear any previous error messages
    });

	 $( ".accessGroupTable" ).each(function( index ) 
	{
		  var check = $(this).find('input[type="checkbox"]');
		  check.bind('click', toggleGroupMembership);
	});

});     

/* 
   Toggle visibility of the appropriate shopper access group table and header after
    the shopper row has been selected in the shoppers grid
*/
function showShopperAccessGroup() 
{
	var shopperID = $(this).find('td:eq(0)').html();
	var currentShopperTableId = "accessGroup" + shopperID.trim() + "_Table";
	var currentShopperHeaderId = "accessGroup" + shopperID.trim() + "_Header";
	
	//loop through all access group tables and hide all the ones except that of the current shopper
	$( ".accessGroupTable" ).each(function( index ) 
	{
		if($(this).is("#" + currentShopperTableId))
			$( this ).css("display","block");
		else
			$( this ).css("display","none");
	});

	//now do the same for the headers
	$( ".accessGroupHeader" ).each(function( index ) 
	{
		if($(this).is("#" + currentShopperHeaderId))
			$( this ).css("display","block");
		else
			$( this ).css("display","none");
	});

	//now the main access group header - always show this as long as any access group grid is visible
	$( ".accessGroupMainHeader" ).css("display","block");
}

/* 
   Toggle shopper access group membership when the checkbox in the shopper access 
   group grid is checked or unchecked
*/
function toggleGroupMembership() 
{
	var isMember = this.checked;
	var accessGroupID = this.value;
	var shopperID = $(this).parent().parent().find('td:eq(0)').html().trim();
	var phpFile = isMember ? "AddShopperAsync.php" : "RemoveShopperAsync.php";

	if(xmlHttp)
	{
		try
		{
		  var params = "shopperID=" + shopperID + "&accessGroupID=" + accessGroupID;

		  xmlHttp.open("POST",phpFile,true);
		  xmlHttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
		  xmlHttp.send(params);
		  xmlHttp.onreadystatechange = handleToggleMembershipResponse;

		  $('.errorMessage').html(""); //make sure error message is cleared
		  currentCheckBox =  $(this);
		  currentCheckBox.prop("disabled", true);
		  ShowProgressAnimation();
		}
		catch(e)
		{
		  alert("Cannot connect to server. Error detail: " + e.toString());
		}
	 }
}

/*
   function to run after the state change when calling xmlHttp.send in 
   toggleGroupMembership function
*/
function handleToggleMembershipResponse()
{
	if (xmlHttp.readyState == 4)
	{
		//re-enable the checkbox and hide the wait cursor
		currentCheckBox.prop("disabled", false);
        HideProgressAnimation();

		//if an error occurred display the error message in red text below the grid
		if(xmlHttp.status == 200)
		{
			  //ready and in correct status
			  try
			  {					
				  if(!xmlHttp.responseText.contains("Success"))
				  {
					  //an error occurred in processing so we must inform the user
					  var errorHtml = "";
					  if(xmlHttp.responseText == null || xmlHttp.responseText == "")
						   errorHtml = "An error occurred processing your request.<br>";
					  else
						   errorHtml = "Error: " + xmlHttp.responseText + "<br>";
			
					  errorHtml += "Your changes have not been saved.<br>";
					  $('.errorMessage').html(errorHtml);

					  //since the change couldn't be saved we have to revert the checkbox 
					  //to its prior state
					  currentCheckBox.prop("checked", !currentCheckBox.prop("checked"));
				  }
			  }
			  catch(e)
			  {
				   $('.errorMessage').html("Error reading the response: " + e.toString() + "<br><br>");
			  }
		}
		else
		{
		    $('.errorMessage').html( "Problem retrieving data:\n" + xmlHttp.statusText + "<br><br>");
		}
	}
}

/* 
  creates XMLHttpRequest Instance
*/
function createXmlHttpRequestObject()
{
  var xmlHttp;
  // works all exceprt IE6 and older  
  try
  { 
    xmlHttp = new XMLHttpRequest();    
  }
  catch(e)
  {
    // assume IE 6 or older
    try
    {
      xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    catch(e){ }
  }
  // return object or display error
  if (!xmlHttp)
    alert("Error creating the XMLHttpRequest Object");
  else
    return xmlHttp;
}
