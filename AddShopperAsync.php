<?php

include 'RBAC.php';

if(isset($_POST['shopperID']) && is_numeric($_POST['shopperID']) )
{	
	if(isset($_POST['accessGroupID']) && is_numeric($_POST['accessGroupID']) )
	{
		$accessGroupID = $_POST['accessGroupID'];
		$shopperID	= $_POST['shopperID'];			
			
		RBAC::init();
		RBAC::addShopper($shopperID, $accessGroupID);
	}
}

?>